﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigEnemy : Enemy
{
    private void FixedUpdate()
    {
        if (myTarget != null)
        {
            ChasePlayer(myTarget);
        }            
    }


    public override void ChasePlayer(Transform target)
    {
        
        Vector3 pos = Vector3.MoveTowards(transform.position, target.position, enemySpeed * Time.fixedDeltaTime);
        myRigidbody.MovePosition(pos);
        //transform.LookAt(myTarget);
    }

    public override void InitializeStats()
    {
        damage = 60;
        maxHealth = 300;
        enemySpeed = 1.2f;
        myValue = 90;
        dropRate = 0.4f;
    }
}
