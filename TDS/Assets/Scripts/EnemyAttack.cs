﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    Enemy myEnemy;
    private float damageCooldown = 1f;
    private float nextDamage = 0.0f;

    private void Start()
    {
        myEnemy = GetComponentInChildren<Enemy>();
    }
  

    private void OnCollisionStay(Collision other)
    {
        PlayerController miPC = other.gameObject.GetComponent<PlayerController>();
        if (other.gameObject.CompareTag("Player") && Time.time > nextDamage)
        {
            nextDamage = Time.time + damageCooldown;
            miPC.TakeDamage(myEnemy.damage);
        }
    }
}
