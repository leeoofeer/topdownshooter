﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPowerUp : MonoBehaviour
{
    private int healAmount;
    [SerializeField]
    private GameObject PS_system;
    Vector3 startPos;
    private void Start()
    {
        healAmount = 40;
        startPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }
    private void OnCollisionEnter(Collision other)
    {
        
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerController miPC = other.gameObject.GetComponent<PlayerController>();
            miPC.HealPlayer(healAmount);
        }
        GameObject psSystem = Instantiate(PS_system, startPos, Quaternion.identity);
        Destroy(gameObject);
    }
}
