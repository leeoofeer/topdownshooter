﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCollition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 0.3f);

        foreach (Collider collider in colliders)
        {
            if(collider.tag == "Wall")
            {
                Destroy(gameObject);
                return;
            }

        }
        if(this.GetComponent<Collider>() !=null)GetComponent<Collider>().enabled = true;

    }

    
}
