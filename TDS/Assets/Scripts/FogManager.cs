﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogManager : MonoBehaviour
{
    public event PlayerOpenDoors OnOpenDoors;
    public delegate void PlayerOpenDoors(bool isOpen);

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            OnOpenDoors?.Invoke(true);
        }
        else
        {
            OnOpenDoors?.Invoke(false);
        }
    }
}
