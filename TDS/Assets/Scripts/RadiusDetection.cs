﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiusDetection : MonoBehaviour
{
    [SerializeField]
    private Enemy myController;


    void Start()
    {
       if(myController== null)
        myController = myController.GetComponent<Enemy>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Debug.Log(other.name);
            myController.myTarget = other.gameObject.transform;
        }
        
    }

}
