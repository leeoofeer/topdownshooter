﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private Transform startPos, EndPos;

    [SerializeField]
    private float movSpeed,lifeSpan;

    public Vector3 newStartPos, newEndPos;

    private Enemy Target;
    private int Damage;

    void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        
        /*startPos = GameObject.Find("gunStart").transform;
        EndPos = GameObject.Find("gunEnd").transform;
        newStartPos = startPos.position;
        newEndPos = EndPos.position;*/
        SelfDestroy(lifeSpan);
        Damage = 30;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position += (newEndPos - newStartPos).normalized * movSpeed * Time.deltaTime;
    }

    private void SelfDestroy(float timer)
    {
        Destroy(gameObject,1f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {

            //Debug.Log("Impacto con: " + collision.gameObject.name);
            Target = collision.gameObject.GetComponentInChildren<Enemy>();          
            Target.TakeDamage(Damage, gameObject.tag);
            Destroy(gameObject);
        }
        else 
        {
            //Debug.Log("Impacto con: " + collision.gameObject.name);
            Destroy(gameObject);
        }
    }



}
