﻿
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


[CustomEditor(typeof(DungeonGenerator))]
public class DungeonGeneratorEditor : Editor
{
       public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();



        DungeonGenerator DG = (DungeonGenerator)target;
        

        GUILayout.BeginHorizontal();
        
        if (GUILayout.Button("Generate Random Maze"))
        {
            DG.MazeGenerator();
        }
        if (GUILayout.Button("Delete Maze"))
        {            
            EraseMaze(DG.transform);
        }

        GUILayout.EndHorizontal();
    }

    public void EraseMaze(Transform father)
    {
        while(father.childCount >= 1)
        {
            foreach (Transform child in father)
            {
                GameObject.DestroyImmediate(child.gameObject);
            }
        }
       
    }

}
