﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class PlayerEventController
{
    public static Action<PlayerController,PlayerInput> OnPlayerControllerActivation;
    public static void PlayerControllerActivation(PlayerController PC, PlayerInput PI)
    {
        OnPlayerControllerActivation?.Invoke(PC,PI);
    }
}


public class PlayerController : MonoBehaviour
{
    public Animator myAmtor;

    [Header("Stats")]
    public int playerNumber;
    public float currentHealth;    
    public float maxHealth = 100;
    public float speed;
    [SerializeField]
    float rotationSpeed = 5;
    [SerializeField]
    public float fireRate = 0.4f;

    private Vector2 movementInput;
    private Vector2 aimInput;
    private float shootInput;
    [SerializeField]
    private GameObject GunPoint;
    private float nextFire = 0.0f;

    [SerializeField]
    private GameObject Bullet;


    [ReadOnly]
    public int goldMoney;
    [SerializeField]
    private GameObject stPos, ndPos;

    public event EventHandler OnShootInput;

    PlayerInputManager PIM;
    PlayerInput PI;

    Material PlayerZeroMat, PlayerOneMat;



    private void Start()
    {
        InitializePlayer();
    }

    private void Update()
    {
        MovementInput();
        ShootInput();
    }

    void InitializePlayer()
    {
        PI = gameObject.GetComponent<PlayerInput>();
        currentHealth = maxHealth;
        OnShootInput += GunShoot;
        //GunPoint = this.transform.Find("Gun").gameObject;
        PlayerEventController.PlayerControllerActivation(this, GetComponent<PlayerInput>());
        PIM = FindObjectOfType<PlayerInputManager>();
        PlayerZeroMat = Resources.Load("PolyartOrange", typeof(Material)) as Material;
        PlayerOneMat = Resources.Load("PolyartCyan", typeof(Material)) as Material;
        FindChilds();
    }
    void FindChilds()
    {
        if (PI.playerIndex == 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject child = transform.GetChild(i).gameObject;
                if (!(child.name == "Hips" || child.name == "Camera"))
                    child.gameObject.GetComponent<Renderer>().material = PlayerZeroMat;
            }
        }
        else
        {
            for(int i = 0; i < transform.childCount; i++)
            {
                GameObject child = transform.GetChild(i).gameObject;
                if (!(child.name == "Hips" || child.name == "Camera"))
                    child.gameObject.GetComponent<Renderer>().material = PlayerOneMat;
            }
        }
    }
    



    private void MovementInput()
    {
        Vector3 movementDirection = new Vector3(movementInput.x, 0, movementInput.y);

        movementDirection.Normalize();
        transform.Translate(movementDirection * speed * Time.deltaTime, Space.World);
        Vector3 direction = new Vector3(aimInput.x, 0, aimInput.y);
        direction.Normalize();
        myAmtor.SetFloat("VelX", movementInput.x, 0.1f, Time.deltaTime);
        myAmtor.SetFloat("VelY", movementInput.y, 0.1f, Time.deltaTime);

        if (direction != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(direction, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }
    }

    private void ShootInput()
    {
        if (shootInput != 0f && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            OnShootInput?.Invoke(this, EventArgs.Empty);
        }
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;    
        if (currentHealth <= 0)
        {            
            Die();
        }
    }        

    public void HealPlayer(int heal)
    {
        if (currentHealth >= 0)
        {
            currentHealth += heal;
            if (currentHealth > maxHealth)
            {
                currentHealth = maxHealth;
            }
        }
    }


    private void GunShoot(object sender, EventArgs e)
    {
        GameObject bull = Instantiate(Bullet, GunPoint.transform.position, Quaternion.identity);
        bull.gameObject.tag = "Player" + PI.playerIndex;
        bull.gameObject.GetComponent<Bullet>().newStartPos = stPos.transform.position;
        bull.gameObject.GetComponent<Bullet>().newEndPos = ndPos.transform.position; 
    }

    private void Die()
    {
        PIM.DisableJoining();
        StartCoroutine(PlayerLoose());        
    }

    IEnumerator PlayerLoose()
    {
        Destroy(gameObject);
        SceneManager.LoadScene(1);
        return new WaitForSecondsRealtime(1f);        
    }

    public void OnMove(InputAction.CallbackContext ctx) => movementInput = ctx.ReadValue<Vector2>();
    public void OnAim(InputAction.CallbackContext aim) => aimInput = aim.ReadValue<Vector2>();
    public void OnShoot(InputAction.CallbackContext sht) => shootInput = sht.ReadValue<float>();
}
