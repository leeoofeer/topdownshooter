﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuController : MonoBehaviour
{
    [Header("Levels to load")]
    public string mainGameLevel;

    [Header("Gameplay Settings")]
    public string asd;

    [Header("Volume Settings")]
    [SerializeField] private TMP_Text volumeValue = null;
    [SerializeField] private Slider volumeSlider = null;
    [SerializeField] private float defaultVolumeValue = 0.5f;

    [Header("Graphic Settings")]
    [SerializeField] private Slider brightnessSlider = null;
    [SerializeField] private TMP_Text brightnessValue = null;
    [SerializeField] private float defaultBrightness = 1;
    [SerializeField] private Toggle fullscreenToggle = null;

    [Header("Resolutions Dropdown")]
    public TMP_Dropdown resolutionDropdown;
    public TMP_Dropdown qualityDropdown;
    private Resolution[] resolutions;


    [SerializeField] private GameObject confirmationPrompt = null;

    private int currenResolutionIndex = 0;
    private int qualityLevel;
    private bool isFullScreen;
    private float brightnessLevel;
    private void Start()
    {
        InitializeAudio();
        InitializeGraphics();
    }

    private void InitializeGraphics()
    {
        //
        //Graphics
        // - here update brightness value with playerprefs saved value
        brightnessSlider.value = PlayerPrefs.GetFloat("Brightness", brightnessLevel);
        brightnessValue.text = PlayerPrefs.GetFloat("Brightness", brightnessLevel).ToString("0.0");
        qualityDropdown.value = PlayerPrefs.GetInt("QualityLevel", qualityLevel);
        qualityDropdown.RefreshShownValue();
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("QualityLevel", qualityLevel));
        bool savedFullScreen;
        if (PlayerPrefs.GetInt("FullScreen") == 1)
        { savedFullScreen = true; }
        else
        { savedFullScreen = false; }
        Screen.fullScreen = savedFullScreen;
        fullscreenToggle.isOn = savedFullScreen;
        //      

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();


        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);
            if (resolutions[i].width == Screen.width && resolutions[i].height == Screen.height)
            {
                currenResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currenResolutionIndex;

        resolutionDropdown.RefreshShownValue();
    }

    private void InitializeAudio()
    {
        //Update settings on Start
        //
        //Audio
        AudioListener.volume = PlayerPrefs.GetFloat("Volume");
        volumeSlider.value = PlayerPrefs.GetFloat("Volume");
        volumeValue.text = PlayerPrefs.GetFloat("Volume").ToString("0.00");
    }


    public void NewGameDialogYes()
    {
        SceneManager.LoadScene(mainGameLevel);
    }

    #region GraphicsSettings
    public void SetBrightness(float brightness)
    {
        brightnessLevel = brightness;
        brightnessValue.text = brightness.ToString("0.0");
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetFullScreen(bool isFS)
    {
        isFullScreen = isFS;
    }

    public void SetQuality(int qualityIndex)
    {
        qualityLevel = qualityIndex;
    }

    public void GraphicsApply()
    {
        PlayerPrefs.SetFloat("Brightness", brightnessLevel);
        //Change brightness with postprocessing or something

        PlayerPrefs.SetInt("QualityLevel", qualityLevel);
        QualitySettings.SetQualityLevel(qualityLevel);

        PlayerPrefs.SetInt("FullScreen", (isFullScreen ? 1 : 0));
        Screen.fullScreen = isFullScreen;

        StartCoroutine(ConfirmationBox());
    }

    public void UpdateGraphicsSettingsOnBack()
    {
        brightnessSlider.value = PlayerPrefs.GetFloat("Brightness", brightnessLevel);
        brightnessValue.text = PlayerPrefs.GetFloat("Brightness", brightnessLevel).ToString("0.0");
        /*
        qualityDropdown.value = PlayerPrefs.GetInt("QualityLevel", qualityLevel);
        qualityDropdown.RefreshShownValue();
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("QualityLevel", qualityLevel));       

        resolutionDropdown.value = currenResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        */
        bool savedFullScreen;
        if (PlayerPrefs.GetInt("FullScreen") == 1)
        { savedFullScreen = true; }
        else
        { savedFullScreen = false; }
        fullscreenToggle.isOn = savedFullScreen; 
    }

    #endregion


    #region SoundSettings
    public void SetVolume(float volume)
    {
        AudioListener.volume = volume;
        volumeValue.text = volume.ToString("0.00");
    }

    public void VolumeApply()
    {
        PlayerPrefs.SetFloat("Volume", AudioListener.volume);
        StartCoroutine(ConfirmationBox());
    }

    public void UpdateVolumeSliderOnBack()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("Volume");
        volumeValue.text = PlayerPrefs.GetFloat("Volume").ToString("0.00");
    }


    #endregion


    #region GeneralBehavior


    public void ResetButton(string MenuType)
    {
        if (MenuType == "Graphics")
        {
            brightnessSlider.value = 1f;
            brightnessValue.text = 1f.ToString("0.0");
            qualityDropdown.value = 2;
            QualitySettings.SetQualityLevel(1);

            
            
            resolutions = Screen.resolutions;
            resolutionDropdown.value = resolutions.Length;
            resolutionDropdown.RefreshShownValue();

            fullscreenToggle.isOn = true;
            Screen.fullScreen = true;
            GraphicsApply();
        }

        if (MenuType == "Audio")
        {
            AudioListener.volume = defaultVolumeValue;
            volumeSlider.value = defaultVolumeValue;
            volumeValue.text = defaultVolumeValue.ToString("0.00");
            VolumeApply();
        }
    }

    public IEnumerator ConfirmationBox()
    {
        confirmationPrompt.SetActive(true);
        yield return new WaitForSeconds(2);
        confirmationPrompt.SetActive(false);
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    #endregion

}
