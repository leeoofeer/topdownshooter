﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public abstract class Enemy : MonoBehaviour
{
    [Header("Linked objects"), SerializeField]
    protected GameObject Father;
    [SerializeField] protected Image hBar;
    [ReadOnly, SerializeField] public Transform myTarget;
    [SerializeField] protected Rigidbody myRigidbody;
    [SerializeField] protected GameObject powerUP;

    [Header("Stats"), DisplayWithoutEdit(), SerializeField]
    protected float enemySpeed;
    [DisplayWithoutEdit(), SerializeField]
    public int damage;
    [DisplayWithoutEdit(), SerializeField]
    protected float maxHealth;
    [DisplayWithoutEdit(), SerializeField]
    float currentHealth;
    [DisplayWithoutEdit(), SerializeField]
    protected int myValue;

    protected MeshRenderer myMesh;
    protected PlayerController myCharacter;
    protected float dropRate;
    uiEventManager uiEM;


    private void Awake()    
    {
        InitializeStats(); 
    }

    void Start()
    {
        InitializeEnemy();      
    }

    public abstract void InitializeStats();
    

    protected virtual void InitializeEnemy()    {

        myMesh = GetComponent<MeshRenderer>();
        if (myRigidbody == null)
            myRigidbody = GetComponentInParent<Rigidbody>();

        currentHealth = maxHealth;
        UpdateHealth();
        myCharacter = FindObjectOfType<PlayerController>();
        uiEM = FindObjectOfType<uiEventManager>();

    }

    public abstract void ChasePlayer(Transform target);

    public virtual void TakeDamage(int damage, string gm)
    {
        currentHealth -= damage; 
        myMesh.material.color = Color.magenta; 
        UpdateHealth();
        if (currentHealth <= 0)
        {
            if (gm == "Player0")
            {
                uiEM.PC1.goldMoney += myValue;

            }else if (gm == "Player1")
            {
                uiEM.PC2.goldMoney += myValue;
            }
             
            Die();
        }
        StartCoroutine(ColorTimer());
    }


    void UpdateHealth(){hBar.fillAmount = currentHealth / maxHealth;}

   

    void Die() 
    {
        float dropChance = Random.Range(0f, 1f);
        if (dropChance <= dropRate)
        {
            Debug.Log(gameObject.name + " Ramdom chance: " + dropChance);
            GameObject HPup = Instantiate(powerUP, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
        }
        Destroy(Father.gameObject);
    }

    IEnumerator ColorTimer()
    {
        yield return new WaitForSecondsRealtime(0.3f);
        myMesh.material.color = Color.red;
    }
}
