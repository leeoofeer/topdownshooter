﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject common;
    [SerializeField]
    private GameObject rare;


    [SerializeField]
    private int childCounter = 20;
    [SerializeField]
    private float spawnTime = 1f;
    [SerializeField]
    private float commonChance;

    void Start()
    {
        if (common != null && rare != null)
        {       
            StartCoroutine(SpawnEnemies(spawnTime));
        }
       
    }

    void Update()
    {
       
    }

    IEnumerator SpawnEnemies(float sTime)
    {
        for (int i = 0; i < childCounter; i++)
        {
            float randomChance = Random.Range(0.0f, 1.0f);
            if (randomChance < commonChance)
            {
                Instantiate(common, new Vector3(Random.Range(transform.position.x + -8, transform.position.x + +8), transform.position.y, Random.Range(transform.position.z - 8, transform.position.z + 8)), Quaternion.identity);
                yield return new WaitForSeconds(sTime);
            }
            else
            {
                Instantiate(rare, new Vector3(Random.Range(transform.position.x + -8, transform.position.x + +8), transform.position.y, Random.Range(transform.position.z - 8, transform.position.z + 8)), Quaternion.identity);
                yield return new WaitForSeconds(sTime);                
            }
        }
    }
}
