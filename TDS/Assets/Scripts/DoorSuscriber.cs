﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSuscriber : MonoBehaviour
{
   public GameObject Spawner = null;
   void Start()
    {
        FogManager fogManager = GetComponent<FogManager>();

        //Collision activate
        fogManager.OnOpenDoors += FogManager_OnOpenDoors;

        Transform myParent = transform.parent;
        foreach (Transform child in myParent)
        {
            if(child.name == "Spawner")
            {
                Spawner = child.gameObject;
            }
        }
    }

    private void FogManager_OnOpenDoors(bool isOpen)
    {
        this.gameObject.SetActive(!isOpen);

        if (!(Spawner == null))
            Spawner.SetActive(isOpen);
    }
}
