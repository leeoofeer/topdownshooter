﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    

    [SerializeField]
    private string myPowerType;
    [SerializeField]
    private int myCost;

   
    Material pSpeed, pHealthUp, pDamage;
    int rnd;
    void Start()
    {
        rnd = Random.Range(1, 4);       
        switch (rnd)
        {
            case 1:
                myCost = 500;
                pSpeed = Resources.Load("pSpeed", typeof(Material)) as Material;
                gameObject.GetComponent<Renderer>().material = pSpeed;
                break;
            case 2:
                myCost = 650;
                pHealthUp = Resources.Load("pHealthUp", typeof(Material)) as Material;
                gameObject.GetComponent<Renderer>().material = pHealthUp;
                break;
            case 3:
                myCost = 850;
                pDamage = Resources.Load("pDamage", typeof(Material)) as Material;
                gameObject.GetComponent<Renderer>().material = pDamage;
                break;
            default:
                break;
        }


    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            PlayerController myPC = other.gameObject.GetComponent<PlayerController>();
            switch (rnd)
            {
                case 1:
                    if (myPC.goldMoney >= myCost)
                    {
                        myPC.speed += 1.5f;
                        myPC.goldMoney -= myCost;
                    }
                    break;
                case 2:
                    if (myPC.goldMoney >= myCost)
                    {
                        myPC.maxHealth += 50;
                        myPC.goldMoney -= myCost;
                    }
                    break;
                case 3:
                    if (myPC.goldMoney >= myCost)
                    {
                        myPC.fireRate -= 0.05f;
                        myPC.goldMoney -= myCost;
                    }
                    break;
                default:
                    Debug.Log("DEFAULT");
                    break;
            }
            Destroy(gameObject);
        }
    }

    
}
