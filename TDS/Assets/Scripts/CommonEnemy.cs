﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonEnemy : Enemy
{
    
    private void FixedUpdate()
    {
        if(myTarget!=null)
            ChasePlayer(myTarget);
    }

    public override void ChasePlayer(Transform target)
    {
        //e chiquito cambia de target cuando entre otro player en el trigger
        Vector3 pos = Vector3.MoveTowards(transform.position, target.position, enemySpeed * Time.fixedDeltaTime);
        myRigidbody.MovePosition(pos);
        //transform.LookAt(myTarget);
    }

    public override void InitializeStats()
    {
        damage = 30;
        maxHealth = 100;
        enemySpeed = 3.2f;
        myValue = 50;
        dropRate = 0.15f;
    }
}
