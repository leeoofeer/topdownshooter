using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour
{
    public class Cell
    {
        public bool visited = false;
        public bool[] status = new bool[4];
    }

    [System.Serializable]
    public class Rule
    {
        public GameObject room;
        public Vector2Int minPosition;
        public Vector2Int maxPosition;

        public int roomQuantity = -1;
        public bool obligatory;
        public int usedAllRooms()
        {
            if (roomQuantity > 0)
            {
                return 1;
            }                
            else if (roomQuantity == 0)
            {
                return 0;
            }
            else if (roomQuantity == -1)
            {
                return 1;
            }
            else { return 0; }
           
        }

        public int ProbabilityOfSpawning(int x, int y)
        {
            // 0 = cannot spawn // 1 = can spawn // 2 = has to spawn
            if (x>=minPosition.x && x<= maxPosition.x && y >= minPosition.y && y <= maxPosition.y)
            {return obligatory ? 2 : 1;} return 0;
        }

    }    

    public Vector2 size;
    public int startPos = 0;
    public Rule[] rooms;
    public Vector2 offset;    
    List<Cell> board;

    void Start()
    {
        MazeGenerator();
    }   

    void GenerateDungeon()
    {
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                Cell currentCell = board[Mathf.FloorToInt(i + j * size.x)];
                if (currentCell.visited)
                {
                    int randomRoom = -1;
                    List<int> availableRooms = new List<int>();
                    for (int k = 0; k < rooms.Length; k++)
                    {
                        int p = rooms[k].ProbabilityOfSpawning(i, j);
                        int h = rooms[k].usedAllRooms();
                        if (p == 2 && h == 1) // If its obligatory = has to spawn
                        {
                            randomRoom = k;
                            if (rooms[k].roomQuantity >= 0)
                            {
                                rooms[k].roomQuantity--;
                            }                            
                            break;
                        }else if (p == 1 && h == 1) // If its not obligatory = may spawn
                        {
                            if (rooms[k].roomQuantity >= 0)
                            {
                                rooms[k].roomQuantity--;
                            }
                            availableRooms.Add(k);
                           
                        }
                    }
                    if(randomRoom == -1)
                    {
                        if (availableRooms.Count > 0)
                        {
                            randomRoom = availableRooms[Random.Range(0, availableRooms.Count)];
                        }
                        else
                        {
                            randomRoom = 0;
                        }
                    }

                    var newRoom = Instantiate(rooms[randomRoom].room, new Vector3(i * offset.x, 0, -j * offset.y), Quaternion.identity, transform).GetComponent<RoomBehaviour>();
                    newRoom.UpdateRoom(currentCell.status);

                    newRoom.name += " " + i + "-" + j;
                }

            }
        }


    }



    public void MazeGenerator()
    {
        board = new List<Cell>();

        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                board.Add(new Cell());
            }
        }

        int currentCell = startPos;

        Stack<int> path = new Stack<int>();

        int k = 0;

        while (k < 1000)
        {
            k++;

            board[currentCell].visited = true;

            if (currentCell == board.Count - 1)
            {
                break;
            }

            //Check near cells
            List<int> nerighbors = CheckNeighbors(currentCell);

            if (nerighbors.Count == 0)
            {
                if (path.Count == 0)
                {
                    break;
                }
                else
                {
                    currentCell = path.Pop();
                }
            }
            else
            {
                path.Push(currentCell);
                int newCell = nerighbors[Random.Range(0, nerighbors.Count)];

                //Check direction
                if (newCell > currentCell)
                {
                    //Down or Right
                    if (newCell - 1 == currentCell)
                    {
                        board[currentCell].status[2] = true;
                        currentCell = newCell;
                        board[currentCell].status[3] = true;
                    }
                    else
                    {
                        board[currentCell].status[1] = true;
                        currentCell = newCell;
                        board[currentCell].status[0] = true;
                    }
                }
                else
                {
                    //Up or Left
                    if (newCell + 1 == currentCell)
                    {
                        board[currentCell].status[3] = true;
                        currentCell = newCell;
                        board[currentCell].status[2] = true;
                    }
                    else
                    {
                        board[currentCell].status[0] = true;
                        currentCell = newCell;
                        board[currentCell].status[1] = true;
                    }

                }
            }

        }
        GenerateDungeon();
    }

    List<int> CheckNeighbors(int cell)
    {
        List<int> neighbors = new List<int>();

        //check UP
        if (cell - size.x >= 0 && !board[Mathf.FloorToInt(cell - size.x)].visited)
        {
            neighbors.Add(Mathf.FloorToInt(cell - size.x));
        }
        //check Down
        if (cell + size.x < board.Count && !board[Mathf.FloorToInt(cell + size.x)].visited)
        {
            neighbors.Add(Mathf.FloorToInt(cell + size.x));
        }
        //check Right
        if ((cell + 1) % size.x != 0 && !board[Mathf.FloorToInt(cell + 1)].visited)
        {
            neighbors.Add(Mathf.FloorToInt(cell + 1));
        }
        //check Left
        if (cell % size.x != 0 && !board[Mathf.FloorToInt(cell - 1)].visited)
        {
            neighbors.Add(Mathf.FloorToInt(cell - 1));
        }



        return neighbors;

    }


}
