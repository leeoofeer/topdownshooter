﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;



public class EnemyController : MonoBehaviour
{
    public NavMeshAgent agent;
    private float maxHealth = 100f;
    private float currentHealth;
    public Image hBar;
    PlayerController myCharacter;
    private void Start()
    {
        currentHealth = maxHealth;
        myCharacter = GameObject.FindObjectOfType<PlayerController>();


    }


    private void OnTriggerStay(Collider other)
    {
        if (other.name == "Character")
            agent.SetDestination(other.transform.position);


    }


    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        UpdateHealth();
        if (currentHealth <= 0)
        {
            //myCharacter.soulLevel += 100;
            //myBonfire.OpenDoors();
            StartCoroutine(SelfDestroy());

        }
    }


    void UpdateHealth()
    {
        hBar.fillAmount = currentHealth / maxHealth;

    }
    IEnumerator SelfDestroy()
    {
        yield return new WaitForSeconds(0.22f);
        Destroy(gameObject);
    }
}
