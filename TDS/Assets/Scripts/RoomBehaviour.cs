﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomBehaviour : MonoBehaviour
{
    public GameObject[] walls; // 0-Up   1-Down   2-Right   3-Left
    public GameObject[] doors;

    public bool[] testStatus;


    void Start()
    {


    }

   public void UpdateRoom(bool[] status)
    {
        for (int i = 0; i < status.Length; i++)
        {
            doors[i].SetActive(status[i]);
            walls[i].SetActive(!status[i]);

        }
    }

   
    public void RandomTestStatus()
    {
        for (int i = 0; i < testStatus.Length; i++)
        {
            testStatus[i] = GenerateRandomBool();
        }
    }

    public bool GenerateRandomBool()
    {
        if (Random.value >= 0.5)
        {
            return true;
        }
        return false;
    }
 

}
