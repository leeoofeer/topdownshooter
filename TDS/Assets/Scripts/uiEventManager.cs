﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;



public class uiEventManager : MonoBehaviour
{
    public GameObject uiPlayerOne = null;
    public GameObject uiBarOne = null;
    public GameObject uiPlayerTwo = null;
    public GameObject uiBarTwo = null;
    public Text GoldTwo = null;
    public Text GoldOne = null;


    private void OnEnable()
    {
        PlayerEventController.OnPlayerControllerActivation += PlayerControllerActivation;
    }

    private void OnDisable()
    {
        PlayerEventController.OnPlayerControllerActivation -= PlayerControllerActivation;
    }

    private void PlayerControllerActivation(PlayerController obj,PlayerInput PI)
    {
        ActivateUI(obj,PI);
    }



    void Start()
    {
        Initialize();

    }

    private void Initialize()
    {
        uiPlayerOne = GameObject.Find("Player1");
        uiPlayerTwo = GameObject.Find("Player2");
        GoldOne = GameObject.Find("Gold1").GetComponent<Text>();
        GoldTwo = GameObject.Find("Gold2").GetComponent<Text>();
        uiPlayerOne.SetActive(false);
        uiPlayerTwo.SetActive(false);

        PlayerController myPController = FindObjectOfType<PlayerController>();
    }

    public Image FillBar1, FillBar2;
    public PlayerController PC1, PC2;

    private void ActivateUI (PlayerController player, PlayerInput PI)
    {        
        
        if (PI.playerIndex == 0)
        {
            PC1 = player;
            PC1.playerNumber = PI.playerIndex;
            uiPlayerOne.SetActive(true);
            Debug.Log("Mi player es: " + PC1.playerNumber);
            FillBar1.fillAmount = PC1.currentHealth / PC1.maxHealth;
            FillBar1.color = new Color(245, 179, 0, 255);
            GoldOne.text = "     " + PC1.goldMoney;
        }
        else
        {
            PC2 = player;
            PC2.playerNumber = PI.playerIndex;
            uiPlayerTwo.SetActive(true);
            Debug.Log("Mi player es: " + PC2.playerNumber);
            FillBar2.fillAmount = PC2.currentHealth / PC2.maxHealth;
            FillBar2.color = new Color(0, 255, 255, 255);
            GoldTwo.text = "     " + PC2.goldMoney;
        }

    }

   

    private void Update()
    {
        UpdateHealthBars();

    }

    private void UpdateHealthBars()
    {
        if (PC1 != null)
        {
            FillBar1.fillAmount = PC1.currentHealth / PC1.maxHealth;
            GoldOne.text = "     " + PC1.goldMoney;

        }

        if (PC2 != null)
        {
            FillBar2.fillAmount = PC2.currentHealth / PC2.maxHealth;
            GoldTwo.text = "     " + PC2.goldMoney;
        }
    }
}
