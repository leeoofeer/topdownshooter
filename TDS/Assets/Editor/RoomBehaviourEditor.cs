﻿
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(RoomBehaviour))]
public class RoomBehaviourEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        RoomBehaviour RB = (RoomBehaviour)target;
        
        if (GUILayout.Button("Generate Random Doors"))
        {
            RB.RandomTestStatus();
            RB.UpdateRoom(RB.testStatus);
        }
    }
}
                                                                                                           