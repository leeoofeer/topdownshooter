﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateHealParticles : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem Ps1;
    [SerializeField]
    private ParticleSystem Ps2;
    // Start is called before the first frame update

    GameObject gb;
    void Start()
    {
        Invoke("SelfDestroy", 1.1f);
        Ps1.Play();
        Ps2.Play();        
    }

    void SelfDestroy()
    {
        Destroy(gameObject);
    }
}
